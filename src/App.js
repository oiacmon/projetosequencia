import {Switch, Route, Link} from 'react-router-dom'
import PG1 from './pagina1'
import PG2 from './pagina2'
import PG3 from './pagina3'
import './App.css';

function App() {
  return (
    <div>
      <header>
        <Link to = '/'>Home</Link>
        <Link to = 'pagina1'> Página 1 </Link>
        <Link to = 'pagina2'> Página 2 </Link>
        <Link to = 'pagina3'> Página 3 </Link>
      </header>
      <main>
        <Switch>
          <Route path = '/'/>
          <Route path = 'pagina1' component = {PG1}/>
          <Route path = 'pagina2' component = {PG2}/>
          <Route path = 'pagina3' component = {PG3}/>
        </Switch>
      </main>
    </div>
  );
}

export default App;
